import React, { Component } from 'react'
import SelectPlayer from './selectPlayer';
import Board from './Board'

class Game extends Component {
    constructor(props) {
        super(props);
        this.state = {
            history: [
                {
                    squares: Array(9).fill(null)
                }
            ],
            stepNumber: 0,
            xIsNext: null,
            xWins: 0,
            oWins: 0,
            newGame: true,
            player: null,
        };
    }

    // componentDidMount() {
    //     const state = JSON.parse(localStorage.getItem('todos'));
    //     if (state) {
    //       this.setState(
    //             state
    //         );
    //     }
    // }

    checkWinner(squares) {
        let lines = [
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, 8],
            [0, 3, 6],
            [1, 4, 7],
            [2, 5, 8],
            [0, 4, 8],
            [2, 4, 6]
        ]
        this.checkMatch(lines, squares)

    }

    checkMatch(lines, squares) {
        for (let i = 0; i < lines.length; i++) {
            const [a, b, c] = lines[i];
            if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
                
                const winner = squares[a];

                if (winner === 'X') {
                    this.setState({
                        xWins: this.state.xWins + 1,
                    })
                } else {
                    this.setState({
                        oWins: this.state.oWins + 1,
                    })
                }

                // to add style class to the winning match
                const winBtn = document.getElementsByClassName('square-btn')
                winBtn[a].classList.add('active')
                winBtn[b].classList.add('active')
                winBtn[c].classList.add('active')

                setTimeout(() => {
                    alert(` Congratulations, Winner ${winner} has won the game`)

                    // to remove added style class to the winning match
                    winBtn[a].classList.remove('active')
                    winBtn[b].classList.remove('active')
                    winBtn[c].classList.remove('active')
                    this.resumeGame()
                }, 500)

                return null;

            }
            
            // if no winner and gotten to last move
            if ((squares[a] && (squares[a] !== squares[b] || squares[a] !== squares[c]))&& this.state.stepNumber === 8 && i === 7) {
                setTimeout(() => {
                    alert('Welldone, no winner. The game ended in a draw. Please, play again.')
                    this.resumeGame()
                }, 500)
            } 
        }

        return null
    }
    
    handleClick(i) {
        if(this.state.player && !this.state.winner) {

            const history = this.state.history.slice(0, this.state.stepNumber + 1);
            const current = history[history.length - 1];
            const squares = current.squares.slice();
    
            squares[i] = this.state.xIsNext === true ? "X" : "O";
            if (squares[i] === 'X') {
                this.setState({xIsNext : false})
            } else {
                this.setState({xIsNext : true})
            }
            
            this.setState({
                history: history.concat([
                    {
                        squares: squares
                    }
                ]),
                stepNumber: history.length,
                newGame: false
            });

            this.checkWinner(squares)
        
        }
        // localStorage.setItem('state', JSON.stringify(this.state))

    }

    // function to set player
    setPlayer(player) {
        if(player === 'true') {
            this.setState({ 
                xIsNext: true,
                player: 'X'
            })
        } else if(player === 'false') {
            this.setState({ 
                xIsNext: false,
                player: 'O'
            })
        }
    }

    // function to restart game
    restartGame() {
        this.setState({
            stepNumber: 0,
            xIsNext: true,
        });
    }
    
    // function to undo moves
    undoMoves() {
        this.setState({
            stepNumber: this.state.stepNumber - 1,
            xIsNext: (this.state.stepNumber % 2) === 0
        });
    }

    // function to reset scores
    resetScore() {
        this.setState({
            xWins: 0,
            oWins: 0
        })
    }

    // function to resume game
    resumeGame() {
        this.setState({
            newGame: false,
            stepNumber: 0,
            xIsNext: true,
        })
    }

       
    render() {
        const history = this.state.history;
        const current = history[this.state.stepNumber];

        return (
               
                <div className="game">
                    <div className="game-board">
                    <Board
                        squares={current.squares}
                        onClick={i => this.handleClick(i)}
                    />
                    </div>
                    <div className="game-info">
                            {/* if a move has been made, show option to restart game */}
                            {!this.state.player ?
                                <SelectPlayer 
                                    setPlayer = {(e) => this.setPlayer(e)}
                                />
                            :
                            <>
                                <div>

                                    {this.state.stepNumber === 0 ?
                                        this.state.newGame === true ? 
                                            <div className='intro-text'>
                                                <p>Welcome to the Tic Tac Toe Game. Have Fun!</p>
                                                <p>First Player: {this.state.player}</p>
                                            </div>
                                            : 
                                            <p>Invite your Friends</p>
                                        
                                        :
                                        <>
                                            <div className='status'>Next player: {this.state.xIsNext ? "X" : "O"}</div>
                                            <div className='action-btn-group'>
                                                <button onClick={() => this.restartGame()}>Restart Game</button>
                                                <button onClick={() => this.undoMoves()}>Undo Move</button>
                                            </div>
                                        </>
                                    }
                                </div>
                                <div className='scoretable'>
                                    <p>ScoreBoard:</p>
                                    <table>
                                        <thead>
                                            <tr>
                                                <th>X</th>
                                                <th>O</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>{this.state.xWins}</td>
                                                <td>{this.state.oWins}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <button onClick={() => this.resetScore()}>Reset Scores</button>
                                </div>
                                </>
                            }
                    </div>
                </div>
        );
    }
}

export default Game
