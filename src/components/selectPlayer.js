import React from 'react'

const selectPlayer = ({setPlayer}) => {

    const handleForm = (e) => {
        e.preventDefault();
        let player = e.target.player.value
        setPlayer(player)
    }

    return (
        <div className='form'>
            <p>Select a player to start:</p>
            <form onSubmit={(e) => handleForm(e)}>
                <div className='form-field'>
                    <label>Player X </label>
                    <input type="radio" name="player" value="true" />
                </div>

                <div className='form-field'>
                    <label>Player O </label>
                    <input type="radio" name="player" value="false" />
                </div>
                <input type="submit" className='btn' value="Start" />
            </form>
            
        </div>
    )
}

export default selectPlayer;
