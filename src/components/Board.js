import React from 'react'
import Square from './Square'

const Board = ({squares, onClick}) => {
    const renderSquare = (i) => {
        return <Square 
            value={squares[i]}
            onClick = {() => onClick(i)}
        />
    }

    const array = [0, 1, 2, 3, 4, 5, 6, 7, 8]

    return (
        <div className='board'>
            {array.map((value,index) => {
              
                return (
                    <div className='square' key={index}>
                        {renderSquare(value)}
                    </div>
                )
            } )}
        </div>
    )
}

export default Board;